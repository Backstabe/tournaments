package participation

import (
	"github.com/satori/go.uuid"

	"tournaments/models"
)

// SponsorshipPreloader helps with avoidng N+1 queries for sponsorships
type SponsorshipPreloader interface {
	Load(participationID []uuid.UUID) ([]*models.Sponsorship, error)
}

// PreloadSponsorships creates new option for repository read methods
func PreloadSponsorships(preloader SponsorshipPreloader) Option {
	return Option{
		func(participations []*models.Participation) error {
			ids := make([]uuid.UUID, len(participations))
			mapping := make(map[uuid.UUID]*models.Participation)

			for index, participation := range participations {
				ids[index] = participation.ID
				mapping[participation.ID] = participation
			}

			sponsorships, err := preloader.Load(ids)
			if err != nil {
				return err
			}

			for _, sponsorship := range sponsorships {
				participation := mapping[sponsorship.ParticipationID]
				participation.Sponsorships = append(participation.Sponsorships, sponsorship)
			}

			return nil
		},
	}
}

package player

import (
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/satori/go.uuid"

	"tournaments/models"
)

// NewRepository creates new player repository
// store can be pg.DB or pg.Tx to execute queries during a transaction
// use pg.Tx as store for select-for-update support
func NewRepository(store orm.DB) Repository {
	return Repository{store}
}

// Repository is a Player CRUD service
type Repository struct {
	store orm.DB
}

// Create creates provided player model
func (repository Repository) Create(player *models.Player) error {
	_, err := repository.store.Model(player).Returning("*").Insert()

	return err
}

// Read reads one player model associated with provided public ID
func (repository Repository) Read(id string) (*models.Player, error) {
	player := &models.Player{PublicID: id}

	err := repository.store.Model(player).Where("public_id = ?public_id").Select()

	return player, err
}

// ReadForUpdate reads player associated with provided public ID
// and locks row by FOR UPDATE statement in transaction (if repository was created with pg.Tx)
func (repository Repository) ReadForUpdate(id string) (*models.Player, error) {
	player := &models.Player{
		PublicID: id,
	}

	err := repository.store.Model(player).Where("public_id = ?public_id").For("UPDATE").Select()

	return player, err
}

// ReadManyForUpdate reads players associated with provided public IDs
// and locks them by FOR UPDATE statement in transaction (if repository was created with pg.Tx)
func (repository Repository) ReadManyForUpdate(ids []string) ([]*models.Player, error) {
	var players []*models.Player

	err := repository.store.Model(&players).Where("public_id IN (?)", pg.In(ids)).For("UPDATE").Select()

	return players, err
}

// ReadManyByIDForUpdate reads players associated with provided IDs
// and locks them by FOR UPDATE statement in transaction (if repository was created with pg.Tx)
func (repository Repository) ReadManyByIDForUpdate(ids []uuid.UUID) ([]*models.Player, error) {
	var players []*models.Player

	err := repository.store.Model(&players).Where("id IN (?)", pg.In(ids)).For("UPDATE").Select()

	return players, err
}

// Update updates provided player model
// columns for update can be customized
// default updatable columns are 'points' and 'updated_at'
func (repository Repository) Update(player *models.Player, columns ...string) error {
	player.UpdatedAt = time.Now()

	if len(columns) == 0 {
		columns = append(columns, "points", "updated_at")
	}

	_, err := repository.store.Model(player).Column(columns...).Update()

	return err
}

package tournament

import (
	"tournaments/models"
)

// NewView creates tournament public representaion
func NewView(tournament *models.Tournament) View {
	return View{
		TournamentID: tournament.PublicID,
		Deposit:      tournament.Threshold,
	}
}

// View is a tournament model public representaion
type View struct {
	TournamentID string `json:"tournamentId"`
	Deposit      uint64 `json:"deposit"`
}

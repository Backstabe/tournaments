class CreateTables < ActiveRecord::Migration[5.1]
  def change
    enable_extension :pgcrypto

    # reversible do |direction|
    #   direction.up do
    #     execute <<~SQL
    #       CREATE SEQUENCE player_public_id;
    #       CREATE SEQUENCE tournament_public_id;
    #     SQL
    #   end

    #   direction.down do
    #     execute <<~SQL
    #       DROP SEQUENCE IF EXISTS player_public_id;
    #       DROP SEQUENCE IF EXISTS tournament_public_id;
    #     SQL
    #   end
    # end

    create_table :players, id: :uuid do |t|
      t.string :public_id, null: false, index: { unique: true } #, default: -> { "'P' || NEXTVAL('player_public_id')" }

      t.integer :points, null: false, default: 0

      t.timestamps default: -> { "CURRENT_TIMESTAMP" }
    end

    create_table :tournaments, id: :uuid do |t|
      t.string :public_id, null: false, index: { unique: true } #, default: -> { "NEXTVAL('tournament_public_id')" }

      t.integer :threshold, null: false, default: 0
      t.integer :deposit,   null: false, default: 0
      t.boolean :finished,  null: false, default: false

      t.integer :participants_count, null: false, default: 0

      t.timestamps default: -> { "CURRENT_TIMESTAMP" }
    end

    create_table :participations, id: :uuid do |t|
      t.references :tournament, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.references :player,     type: :uuid, null: false, foreign_key: { on_delete: :cascade }

      t.string  :public_id, null: false, index: true
      t.boolean :confirmed, null: false, default: false
      t.boolean :winning,   null: false, default: false
      t.integer :prize

      t.integer :backers_count, null: false, default: 0

      t.timestamps default: -> { "CURRENT_TIMESTAMP" }
    end

    create_table :sponsorships, id: :uuid do |t|
      t.references :participation, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.references :backer,        type: :uuid, null: false, foreign_key: { on_delete: :cascade, to_table: :players }

      t.boolean :accepted, null: false, default: false

      t.timestamps default: -> { "CURRENT_TIMESTAMP" }
    end
  end
end

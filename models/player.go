package models

import (
	"errors"
	"time"

	"github.com/satori/go.uuid"
)

// Player is a model of player data
type Player struct {
	ID       uuid.UUID
	PublicID string

	// attributes
	Points uint64

	// relations
	Participations []*Participation
	Sponsorships   []*Sponsorship `pg:",fk:backer_id"`

	// meta information
	CreatedAt time.Time
	UpdatedAt time.Time
}

// AddPoints adds provided amount of points to player balance
func (player *Player) AddPoints(points uint64) {
	player.Points = player.Points + points
}

// RemovePoints removes provided amount of points from player balance
// if player doesn't have enough points to remove error will be returned
func (player *Player) RemovePoints(points uint64) error {
	if player.Points < points {
		return errors.New("not enough points")
	}

	player.Points = player.Points - points

	return nil
}

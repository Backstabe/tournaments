package main

import (
	"context"
	"net"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"go.uber.org/zap"

	"tournaments/api/handlers"
)

var (
	version = "0.0.0"
)

// SetupLogger creates new logger
func SetupLogger() (*zap.Logger, error) {
	if debug, err := strconv.ParseBool(os.Getenv("DEBUG")); err == nil && debug {
		return zap.NewDevelopment()
	}

	return zap.NewProduction()
}

// SetupDatabase creates new database connection
func SetupDatabase(logger *zap.Logger) *pg.DB {
	pg.SetLogger(zap.NewStdLog(logger))

	db := pg.Connect(&pg.Options{
		Addr:     net.JoinHostPort(os.Getenv("DATABASE_HOST"), os.Getenv("DATABASE_PORT")),
		User:     os.Getenv("DATABASE_USERNAME"),
		Password: os.Getenv("DATABASE_PASSWORD"),
		Database: os.Getenv("DATABASE_NAME"),
	})

	db.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
		query, err := event.FormattedQuery()
		if err != nil {
			logger.Error("cant build query", zap.Error(err))
			return
		}

		log := logger.With(
			zap.String("source", event.File+":"+strconv.Itoa(event.Line)),
			zap.Duration("time", time.Since(event.StartTime)),
		)

		if event.Error != nil {
			log.Error(query, zap.Error(err))
			return
		}

		log.Debug(query)
	})

	return db
}

// SetupServer creates and configures new web server
func SetupServer(logger *zap.Logger) *echo.Echo {
	server := echo.New()
	server.Validator = Validator{validator.New()}
	server.Logger.SetLevel(log.OFF)
	server.Use(LoggerMiddleware{logger}.Wrapper)

	return server
}

func main() {
	logger, err := SetupLogger()
	if err != nil {
		panic(err)
	}

	logger = logger.With(zap.String("version", version))
	webLogger := logger.Named("HTTP")
	sqlLogger := logger.Named("SQL")

	db := SetupDatabase(sqlLogger)
	server := SetupServer(webLogger)

	server.GET("/take", handlers.NewTakeRequestHandler(db, webLogger).Serve)
	server.GET("/fund", handlers.NewFundRequestHandler(db, webLogger).Serve)
	server.GET("/balance", handlers.NewBalanceRequestHandler(db, webLogger).Serve)
	server.GET("/announceTournament", handlers.NewAnnounceTournamentRequestHandler(db, webLogger).Serve)
	server.GET("/joinTournament", handlers.NewJoinTournamentRequestHandler(db, webLogger).Serve)
	server.POST("/resultTournament", handlers.NewResultTournamentRequestHandler(db, webLogger).Serve)
	// server.GET("/reset", func(response http.ResponseWriter, request *http.Request) {
	// })

	address := net.JoinHostPort(os.Getenv("BIND_HOST"), os.Getenv("BIND_PORT"))

	go func() {
		if err := server.Start(address); err != nil {
			logger.Error("shutting down", zap.Error(err))
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		logger.Fatal("shutdown failed", zap.Error(err))
	}
}

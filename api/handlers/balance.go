package handlers

import (
	"net/http"

	"github.com/go-pg/pg"
	"github.com/labstack/echo"
	"go.uber.org/zap"

	"tournaments/entities/player"
)

// NewBalanceRequestHandler creates new BalanceRequestHandler
// provided logger will be wrapped into 'balance' namespace
func NewBalanceRequestHandler(database *pg.DB, logger *zap.Logger) BalanceRequestHandler {
	return BalanceRequestHandler{
		BaseHandler{logger.Named("balance")},
		player.NewRepository(database),
	}
}

// BalanceRequestHandler handles requests for player balance
type BalanceRequestHandler struct {
	BaseHandler
	players player.Repository
}

// BalanceRequestParameters contains parameters
type BalanceRequestParameters struct {
	PlayerID string `query:"playerId" json:"playerId" validate:"required"`
}

// Serve serves incoming requests
func (handler BalanceRequestHandler) Serve(ctx echo.Context) error {
	params := new(BalanceRequestParameters)

	if err := ctx.Bind(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return err
	}

	if err := ctx.Validate(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return handler.HandleValidationErrors(ctx, err)
	}

	playerModel, err := handler.players.Read(params.PlayerID)
	if err != nil {
		handler.logger.Error("getting player has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	return ctx.JSON(http.StatusOK, player.NewView(playerModel))
}

package handlers

import (
	"net/http"

	"github.com/go-pg/pg"
	"github.com/labstack/echo"
	"go.uber.org/zap"

	"tournaments/entities/tournament"
	"tournaments/models"
)

// NewAnnounceTournamentRequestHandler creates new AnnounceTournamentRequestHandler
// provided logger will be wrapped into 'announce' namespace
func NewAnnounceTournamentRequestHandler(database *pg.DB, logger *zap.Logger) AnnounceTournamentRequestHandler {
	return AnnounceTournamentRequestHandler{BaseHandler{logger.Named("announce")}, database}
}

// AnnounceTournamentRequestHandler creates new tournaments
type AnnounceTournamentRequestHandler struct {
	BaseHandler
	store *pg.DB
}

// AnnounceTournamentRequestParameters contains parameters for a new tournament announcement
type AnnounceTournamentRequestParameters struct {
	PublicID  string `query:"tournamentId" validate:"required"`
	Threshold uint64 `query:"deposit" validate:"gt=0"`
}

// Serve serves incoming requests
func (handler AnnounceTournamentRequestHandler) Serve(ctx echo.Context) error {
	params := new(AnnounceTournamentRequestParameters)

	if err := ctx.Bind(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return err
	}

	if err := ctx.Validate(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return handler.HandleValidationErrors(ctx, err)
	}

	tournamentModel := &models.Tournament{
		PublicID:  params.PublicID,
		Threshold: params.Threshold,
	}

	transaction, err := handler.store.Begin()
	if err != nil {
		handler.logger.Error("creating transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	tournamentRepository := tournament.NewRepository(transaction)

	if err := tournamentRepository.Create(tournamentModel); err != nil {
		handler.logger.Error("creating tournament has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	if err := transaction.Commit(); err != nil {
		handler.logger.Error("transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	return ctx.JSON(http.StatusOK, tournament.NewView(tournamentModel))
}

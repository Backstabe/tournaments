package handlers

import (
	"net/http"

	"github.com/go-pg/pg"
	"github.com/labstack/echo"
	"go.uber.org/zap"

	"tournaments/entities/player"
)

// NewTakeRequestHandler creates new TakeRequestHandler
// provided logger will be wrapped into 'take' namespace
func NewTakeRequestHandler(database *pg.DB, logger *zap.Logger) TakeRequestHandler {
	return TakeRequestHandler{BaseHandler{logger.Named("take")}, database}
}

//TakeRequestHandler takes points from player balances
type TakeRequestHandler struct {
	BaseHandler
	store *pg.DB
}

// TakeRequestParameters contains parameters
type TakeRequestParameters struct {
	PlayerID string `query:"playerId" validate:"required"`
	Points   uint64 `query:"points" validate:"gt=0"`
}

// Serve serves incoming requests
func (handler TakeRequestHandler) Serve(ctx echo.Context) error {
	params := new(TakeRequestParameters)

	if err := ctx.Bind(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return err
	}

	if err := ctx.Validate(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return handler.HandleValidationErrors(ctx, err)
	}

	transaction, err := handler.store.Begin()
	if err != nil {
		handler.logger.Error("creating transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	playerRepository := player.NewRepository(transaction)

	player, err := playerRepository.ReadForUpdate(params.PlayerID)
	if err != nil {
		handler.logger.Error("getting player has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	if err := player.RemovePoints(params.Points); err != nil {
		handler.logger.Error("taking points has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	if err := playerRepository.Update(player); err != nil {
		handler.logger.Error("updating player has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	if err := transaction.Commit(); err != nil {
		handler.logger.Error("transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	return nil
}

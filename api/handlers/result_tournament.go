package handlers

import (
	"net/http"

	"github.com/go-pg/pg"
	"github.com/labstack/echo"
	"go.uber.org/zap"

	"tournaments/entities/participation"
	"tournaments/entities/player"
	"tournaments/entities/sponsorship"
	"tournaments/entities/tournament"
)

// NewResultTournamentRequestHandler creates new ResultTournamentRequestHandler
// provided logger will be wrapped into 'result' namespace
func NewResultTournamentRequestHandler(database *pg.DB, logger *zap.Logger) ResultTournamentRequestHandler {
	return ResultTournamentRequestHandler{BaseHandler{logger.Named("result")}, database}
}

// ResultTournamentRequestHandler applies tournament results
type ResultTournamentRequestHandler struct {
	BaseHandler
	store *pg.DB
}

// ResultTournamenRequesttParameters contains tournament results
type ResultTournamenRequesttParameters struct {
	TournamentID string `json:"tournamentId" validate:"required"`
	Winners      []struct {
		Prize    uint64 `json:"prize" validate:"gt=0"`
		PublicID string `json:"playerId" validate:"required"`
	} `json:"winners" validate:"required,min=1"`
}

// Serve serves incoming requests
func (handler ResultTournamentRequestHandler) Serve(ctx echo.Context) error {
	params := new(ResultTournamenRequesttParameters)

	if err := ctx.Bind(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return err
	}

	if err := ctx.Validate(params); err != nil {
		handler.logger.Warn("wrong params", zap.Error(err))
		return handler.HandleValidationErrors(ctx, err)
	}

	transaction, err := handler.store.Begin()
	if err != nil {
		handler.logger.Error("creating transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	playerRepository := player.NewRepository(transaction)
	tournamentRepository := tournament.NewRepository(transaction)
	sponsorhipRepository := sponsorship.NewRepository(transaction)

	tournamentModel, err := tournamentRepository.ReadForUpdate(params.TournamentID)
	if err != nil {
		handler.logger.Error("getting tournament has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	if err := tournamentModel.Finish(); err != nil {
		handler.logger.Error("finishing tournament has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	participationRepository := participation.NewRepository(transaction,
		participation.InTournament(tournamentModel.ID),
	)

	ids := make([]string, len(params.Winners))
	mapping := make(map[string]uint64)

	for index, winner := range params.Winners {
		ids[index] = winner.PublicID
		mapping[winner.PublicID] = winner.Prize
	}

	playerPreloader := player.NewPreloader(playerRepository.ReadManyByIDForUpdate)

	participations, err := participationRepository.ReadManyForUpdate(ids,
		participation.PreloadPlayers(playerPreloader),
		participation.PreloadSponsorships(
			sponsorship.NewPreloader(
				sponsorhipRepository.ReadManyByParticipationID,
				sponsorship.PreloadBackers(playerPreloader),
			),
		),
	)

	if err != nil {
		handler.logger.Error("getting winners has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	for _, participationModel := range participations {
		participationModel.Winning = true

		playerReward := mapping[participationModel.PublicID]

		if participationModel.BackersCount > 0 {
			backerReward := playerReward / (participationModel.BackersCount + 1)
			playerReward = backerReward + (playerReward - backerReward*(participationModel.BackersCount+1))

			for _, sponsorshipModel := range participationModel.Sponsorships {
				backer := sponsorshipModel.Backer

				backer.AddPoints(backerReward)

				if err := playerRepository.Update(backer); err != nil {
					handler.logger.Error("updating backer has failed", zap.Error(err))
					if err := transaction.Rollback(); err != nil {
						handler.logger.Error("transaction has failed", zap.Error(err))
					}

					return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
				}
			}
		}

		player := participationModel.Player

		player.AddPoints(playerReward)

		if err := playerRepository.Update(player); err != nil {
			handler.logger.Error("updating player has failed", zap.Error(err))
			if err := transaction.Rollback(); err != nil {
				handler.logger.Error("transaction has failed", zap.Error(err))
			}

			return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
		}

		if err := participationRepository.Update(participationModel); err != nil {
			handler.logger.Error("updating participation has failed", zap.Error(err))
			if err := transaction.Rollback(); err != nil {
				handler.logger.Error("transaction has failed", zap.Error(err))
			}

			return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
		}
	}

	if err := tournamentRepository.Update(tournamentModel, "completed"); err != nil {
		handler.logger.Error("updating tournament has failed", zap.Error(err))
		if err := transaction.Rollback(); err != nil {
			handler.logger.Error("transaction has failed", zap.Error(err))
		}

		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	if err := transaction.Commit(); err != nil {
		handler.logger.Error("transaction has failed", zap.Error(err))
		return echo.NewHTTPError(http.StatusInternalServerError, "Something goes wrong")
	}

	return nil
}

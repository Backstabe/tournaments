package main

import (
	"time"

	"github.com/labstack/echo"
	"go.uber.org/zap"
)

// LoggerMiddleware provides zap based logger for echo web
type LoggerMiddleware struct {
	logger *zap.Logger
}

// Wrapper wraps handlers into middleware function
func (middleware LoggerMiddleware) Wrapper(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		request := ctx.Request()
		response := ctx.Response()

		contentLength := request.Header.Get(echo.HeaderContentLength)
		if contentLength == "" {
			contentLength = "0"
		}

		middleware.logger.Info("started "+request.Method+" "+request.URL.Path,
			zap.String("uri", request.RequestURI),
			zap.String("method", request.Method),
			zap.String("bytes_in", contentLength),
			zap.String("agent", request.UserAgent()),
			zap.String("remote", ctx.RealIP()),
		)

		start := time.Now()

		defer func() {
			log := middleware.logger.With(
				zap.Int("status", response.Status),
				zap.String("uri", request.RequestURI),
				zap.String("method", request.Method),
				zap.Duration("time", time.Now().Sub(start)),
				zap.Int64("bytes_out", response.Size),
			)

			if err != nil {
				log.Warn("finished "+request.Method+" "+request.URL.Path, zap.Error(err))
				return
			}

			log.Info("finished " + request.Method + " " + request.URL.Path)
		}()

		if err = next(ctx); err != nil {
			ctx.Error(err)
		}

		return
	}
}

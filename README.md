# Tournaments

Social tournaments HTTP API

## Usage

To run application you can use `docker-compose` command:

```bash
docker-compose up
# or
rake run:docker
```

## Setup

> make sure you have `go` and `ruby` installed on your system.

### configuration

All configuration is provided in `.env` files. Development and test environments have additional configs in `.env.development` and `.env.test` files accordingly. To override them with local configuration `.env.local`, `.env.development.local` and `.env.test.local` should be created.

These ENV variables are supported:
* DATABASE_NAME
* DATABASE_HOST
* DATABASE_PORT
* DATABASE_USERNAME
* DATABASE_PASSWORD
* BIND_HOST
* BIND_PORT
* DEBUG

### database

Application runs against PostgreSQL database.
To setup database localy run

```shell
bundle install
bundle exec rake db:setup
```

## Build

All build scripts are concentrated in `Rakefile`.

To build application locally run

```shell
rake build
# or
rake build:app
```

Docker image can be created by running

```shell
rake build:image
```

## Run

To start application run

```shell
rake run
# or
rake run:app
```
